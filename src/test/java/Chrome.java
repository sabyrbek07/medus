import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class Chrome {

    WebDriver driver;


    @BeforeClass
    public void setup() {
        WebDriverManager.chromedriver().setup();


    }

    @BeforeMethod
    public void instantiated() {
        driver = new ChromeDriver();


    }

        @Test
        public void newPost() throws InterruptedException {
        driver.get("http://imgur.com");
        WebElement plus = driver.findElement(By.xpath("//a[@class='ButtonLink Button upload']"));
        plus.click();
        String currentUrl = driver.getCurrentUrl();
        WebElement image = driver.findElement(By.xpath("//img[@class='upload-giraffe']"));
        Thread.sleep(2000);
        Actions action = new Actions(driver);
        WebElement target = driver.findElement(By.xpath("//div[@class='drag-drop-box']"));
        action.dragAndDrop(image,target).perform();
        String newUrl = driver.getCurrentUrl();
        Assert.assertNotEquals(currentUrl,newUrl);


    }

        @Test
        public void search()  {
        driver.get("http://imgur.com");
        driver.findElement(By.xpath("//input[@class='Searchbar-textInput']")).sendKeys("wooden spoon"+ Keys.ENTER);
        List<WebElement> image = driver.findElements(By.xpath("//div[@id='imagelist']//div[@class='post']/div/p/span"));
        WebElement search = driver.findElement(By.xpath("//span[@class='sorting-text-align']/i"));
        String result = search.getText();
        int resInt = Integer.parseInt(result);
        Assert.assertEquals(image.size(),resInt);

    }


    @Test
    public void randomPost() throws InterruptedException {
        driver.get("http://imgur.com");
        WebElement dropdown = driver.findElement(By.xpath("(//div[@class='Dropdown-title'])[2]/span"));
        dropdown.click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("(//div[@class='Dropdown-list'])[2]/div[3]")).click();

    }



}
